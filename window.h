/****************************************************************************
**
** Copyright (C) 2008-2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Qt Software Information (qt-info@nokia.com)
**
** This file is part of the Itemviews NG project on Trolltech Labs.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 or 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.fsf.org/licensing/licenses/info/GPLv2.html and
** http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at qt-sales@nokia.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef WINDOW_H
#define WINDOW_H

#include <qgraphicsitem.h>
#include <qsharedmemory.h>

class Server;

class Window : public QGraphicsItem
{
public:
    enum WindowType {
        ServerWindow,
        ClientWindow
    };

    Window(Server *server, Window *parent = 0, WindowType type = ClientWindow);
    ~Window();

    quint32 id() const;

    QRectF geometry() const;
    void setGeometry(const QRectF &rect);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    bool sceneEvent(QEvent *event);

private:
    WindowType m_type;
    QSizeF m_size;
    QSharedMemory m_surface;
    Server *m_server;
};

#endif//WINDOW_H
